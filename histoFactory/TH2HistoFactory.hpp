#include <vector>
#include <string>
#include <TH2F.h>
#include <TFile.h>
#include <utility>

class TH2FFactory {
private:
    std::vector<std::pair<int, int>> modulePairs;

public:
    std::vector<TH2F*> histogram_vector;

    TH2FFactory();  // Costruttore

    ~TH2FFactory();  // Distruttore

    TH2F* createHistogram(const char* title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup);

    std::vector<TH2F*> createHistogramsFromEnv(const int NModules, const char* base_title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup);

    std::vector<TH2F*> getHistograms();

    void generatePairs(int num_modules) {
        for (int i = 0; i < num_modules; i++) {
            for (int j = i + 1; j < num_modules; j++) {
                modulePairs.push_back(std::make_pair(i, j));
            }
        }
    }

    auto getPairs() {
        return modulePairs;
    }

    void saveHistograms(std::string outfileName);

    std::vector<TH2F *> getHisto();
    

};
