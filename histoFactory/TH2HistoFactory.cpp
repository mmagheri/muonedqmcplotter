#include <cstdlib>  // For std::getenv
#include <iostream>
#include <sstream>  // For std::stringstream
#include "TH2HistoFactory.hpp"

TH2FFactory::TH2FFactory() { }

TH2FFactory::~TH2FFactory() {  
    for(auto hist : histogram_vector) {
        delete hist;
    }
}

TH2F* TH2FFactory::createHistogram(const char* title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup) {
    TH2F* hist = new TH2F(title, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
    histogram_vector.push_back(hist);
    return hist;
}

std::vector<TH2F*> TH2FFactory::createHistogramsFromEnv(const int NModules, const char* base_title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup) {
    std::vector<TH2F*> new_histograms;
    
    for(size_t i = 0; i <= modulePairs.size(); ++i) {
        std::stringstream name, title;
        title << base_title << "_" << modulePairs[i].first << "_" << modulePairs[i].second;
        TH2F* hist = createHistogram(title.str().c_str(), nbinsx, xlow, xup, nbinsy, ylow, yup);
        new_histograms.push_back(hist);
    }
    return new_histograms;
}

std::vector<TH2F*> TH2FFactory::getHistograms() {
    return histogram_vector;
}

void TH2FFactory::saveHistograms(std::string outfileName) {
    TFile* outfile = new TFile(outfileName.c_str(), "RECREATE");
    for(auto hist : histogram_vector) {
        hist->Write();
    }
    outfile->Close();
}

std::vector<TH2F *> TH2FFactory::getHisto() {
    return histogram_vector;
}
