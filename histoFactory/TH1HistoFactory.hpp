#pragma once
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>

class TH1FFactory {
private:
    std::vector<TH1F*> histogram_vector;
public:
    TH1FFactory();  // Costruttore

    ~TH1FFactory();  // Distruttore

    TH1F* createHistogram(const char* title, Int_t nbinsx, Double_t xlow, Double_t xup);

    std::vector<TH1F*> createHistogramsFromEnv(const int NModules, const char* base_title, Int_t nbinsx, Double_t xlow, Double_t xup);

    std::vector<TH1F*> getHistograms();

    void saveHistograms(std::string outfileName);

    std::vector<TH1F *> getHisto();
};
