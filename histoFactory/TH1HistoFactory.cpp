#include <cstdlib>  // For std::getenv
#include <iostream>
#include <sstream>  // For std::stringstream
#include "TH1HistoFactory.hpp"

TH1FFactory::TH1FFactory() { }

TH1FFactory::~TH1FFactory() {  
    for(auto hist : histogram_vector) {
        delete hist;
    }
}

TH1F* TH1FFactory::createHistogram(const char* title, Int_t nbinsx, Double_t xlow, Double_t xup) {
    TH1F* hist = new TH1F(title, title, nbinsx, xlow, xup);
    histogram_vector.push_back(hist);
    return hist;
}

std::vector<TH1F*> TH1FFactory::createHistogramsFromEnv(const int NModules, const char* base_title, Int_t nbinsx, Double_t xlow, Double_t xup) {
    std::vector<TH1F*> new_histograms;
    for(int i = 1; i <= NModules; ++i) {
        std::stringstream title;
        title << base_title << "_" << i;
        TH1F* hist = createHistogram( title.str().c_str(), nbinsx, xlow, xup);
        new_histograms.push_back(hist);
    }
    return new_histograms;
}

std::vector<TH1F*> TH1FFactory::getHistograms() {
    return histogram_vector;
}

void TH1FFactory::saveHistograms(std::string outfileName) {
    TFile* outfile = new TFile(outfileName.c_str(), "RECREATE");
    for(auto hist : histogram_vector) {
        hist->Write();
    }
    outfile->Close();
}

std::vector<TH1F *> TH1FFactory::getHisto() {
    return histogram_vector;
}