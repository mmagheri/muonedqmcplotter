
#include <vector>
#include <fstream>
#include "parser.hpp"
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <filesystem>
#include <iostream>
#include "TH1HistoFactory.hpp"
#include "TH2HistoFactory.hpp"

struct Stub {
    unsigned int superID;
    unsigned short bx;
    unsigned short link;
    float bend;
    float localX;
    float localY;
};

struct Module {
    std::vector<Stub> stubs;
};

std::string prePrendtoFilename(const std::string& filePath, const std::string& prepend) {
    std::filesystem::path path(filePath);
    std::string filename = prepend + path.filename().string();
    std::filesystem::path newPath = path.parent_path() / filename;
    return newPath.string();
}

const int NMODULES = 12;

void readTree(std::string infileName, std::string outfileName) {

    //setup ttree readout
    std::vector<unsigned int>   *SuperID = 0;
	std::vector<unsigned short> *Bx      = 0;
	std::vector<unsigned short> *Link    = 0;
	std::vector<float> *Bend   = 0;
	std::vector<float> *LocalX = 0;
	std::vector<float> *LocalY = 0;

    TFile * f = new TFile(infileName.c_str(), "READ");
    TTree* eventTree = (TTree*) f->Get("cbmsim");
    eventTree->SetBranchAddress("SuperID", &SuperID);
	eventTree->SetBranchAddress("Bx",      &Bx);
	eventTree->SetBranchAddress("Link",    &Link);
	eventTree->SetBranchAddress("Bend",    &Bend);
	eventTree->SetBranchAddress("LocalX",  &LocalX);
	eventTree->SetBranchAddress("LocalY",  &LocalY);


    Module modules[NMODULES]; //declaration of module

    //Create histo factory and histograms
    TH1FFactory* histoFactory = new TH1FFactory();
    TH2FFactory* histoFactory2 = new TH2FFactory();
    histoFactory2->generatePairs(NMODULES);
    auto beamProfiles   = histoFactory->createHistogramsFromEnv(NMODULES, "beamProfile", 200, 0, 1024);
    auto link           = histoFactory->createHistogramsFromEnv(NMODULES, "link", NMODULES, 0, NMODULES);
    auto bend           = histoFactory->createHistogramsFromEnv(NMODULES, "bend", 30, -15, 15);
    auto bx             = histoFactory->createHistogramsFromEnv(NMODULES, "bx", 3564, 0, 3564);
    auto cic            = histoFactory->createHistogramsFromEnv(NMODULES, "cic", 2, 0, 1);
    auto BiProfiles     = histoFactory2->createHistogramsFromEnv(NMODULES, "2DProfile", 200, 0, 1024, 200, 0, 1024);

    for(auto & p: histoFactory2->getPairs()) {
        std::cout << p.first << " " << p.second << std::endl;
    }

    for (int entry = 0; entry < eventTree->GetEntries(); ++entry) {

        eventTree->GetEntry(entry);
        if(entry%100000 == 0) {
            std::cout << "Entry: " << entry << std::endl;
        }
        if(entry == 100000)break;
        //filling the modules
        
        for (size_t j = 0; j < SuperID->size(); ++j) {
            Stub tmpStub;
            tmpStub.superID = SuperID->at(j);
            tmpStub.bend = Bend->at(j);
            tmpStub.bx = Bx->at(j);
            tmpStub.link = Link->at(j);
            tmpStub.localX = LocalX->at(j);
            tmpStub.localY = LocalY->at(j);
            modules[Link->at(j)].stubs.push_back(tmpStub);
        }

        for(int moduleIndex = 0; moduleIndex < NMODULES; ++moduleIndex) {
            for(auto& stub : modules[moduleIndex].stubs){
                beamProfiles[moduleIndex]->Fill(stub.localX);
                link[moduleIndex]->Fill(stub.link);
                bend[moduleIndex]->Fill(stub.bend);
                bx[moduleIndex]->Fill(stub.bx);
                cic[moduleIndex]->Fill(stub.localY);
            }
        }
        //loop over two modules
        int indexOverPairs = 0;
        for(auto & p: histoFactory2->getPairs()) {
            for(auto& stub1 : modules[p.first].stubs) {
                for(auto& stub2 : modules[p.second].stubs) {
                    BiProfiles[indexOverPairs]->Fill(stub1.localX, stub2.localX);
                }
            }
            indexOverPairs++;
        }

        for (int j = 0; j < NMODULES; ++j) {
            modules[j].stubs.clear();
        }
    }

    TFile *outfile = new TFile(outfileName.c_str(), "RECREATE");
    for(auto& h : histoFactory->getHistograms()) {
        h->Write();
    }
    for(auto& h : histoFactory2->getHistograms()) {
        h->Write();
    }
    outfile->Close();

    //histoFactory->saveHistograms(outfileName);
    //histoFactory2->saveHistograms(prePrendtoFilename(outfileName, "2D_"));
}

int main(int argc, char *argv[]) {
    std::string inputFileName = "../testInput/3090_0147273e0_01472b3df_4201232.root";
    if(cmdOptionExists(argv, argv+argc, "-i")){
        inputFileName = getCmdOption(argv, argv + argc, "-i");
    }
    std::string outputFileName = "../outputHisto/prova.root";
    if(cmdOptionExists(argv, argv+argc, "-o")){
        outputFileName = getCmdOption(argv, argv + argc, "-o");
    }

    readTree(inputFileName, outputFileName);
    return 1;
}